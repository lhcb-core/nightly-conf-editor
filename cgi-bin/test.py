#!/usr/bin/python

import cgi
import cgitb
cgitb.enable()

import os
import json

output = json.dumps(dict(os.environ))

print "Content-type: application/json\n"

print output




def prettyPrintValidXML(xml):
    ''' Pretty prints an xml string, all data in the tags is deleted'''
    TAB = '\t'
    NEWLINE = '\r\n'
    TYPE_COMMENT=1
    TYPE_SIMPLETAG=2
    TYPE_COMPLEXTAG_BEGIN=3
    TYPE_COMPLEXTAG_END=4
    TYPE_HEADER=5
    TYPE_ENDOFFILE=0

    def nextTag(xml):
        try:
            index=xml.index('<')
        except ValueError:
            return [TYPE_ENDOFFILE,'','']
        
        if (xml[index+1:index+4]=='!--'):
            # COMMENTS
            commentEnd=xml.index('-->')
            return [TYPE_COMMENT,xml[index:commentEnd+3],xml[commentEnd+3:]]
        else:
            # TAG
            tagEnd=xml.index('>')
            if xml[tagEnd-1]=='/':
                # SIMPLETAG
                return [TYPE_SIMPLETAG,xml[index:tagEnd+1],xml[tagEnd+1:]]
            elif xml[index+1]=='/':
                # COMPLEXTAG_END
                return [TYPE_COMPLEXTAG_END,xml[index:tagEnd+1],xml[tagEnd+1:]]
            elif xml[index+1]=='?' and xml[tagEnd-1]=='?':
                # HEADER TAG
                return [TYPE_HEADER,xml[index:tagEnd+1],xml[tagEnd+1:]]
            else:
                # COMPLEXTAG_BEGIN
                return [TYPE_COMPLEXTAG_BEGIN,xml[index:tagEnd+1],xml[tagEnd+1:]]

    depth=0
    data=xml
    result=''
    while True:
        tag=nextTag(data)
        data=tag[2]
        if tag[0]==TYPE_ENDOFFILE:
            return result
        elif tag[0]==TYPE_SIMPLETAG:
            result += NEWLINE + TAB*depth + tag[1]
        elif tag[0]==TYPE_COMPLEXTAG_BEGIN:
            result += NEWLINE + TAB*depth + tag[1]
            depth +=1
        elif tag[0]==TYPE_COMPLEXTAG_END:
            depth -=1
            result += NEWLINE + TAB*depth + tag[1]
        elif tag[0]==TYPE_COMMENT:
            result += NEWLINE + TAB*depth + tag[1]
        elif tag[0]==TYPE_HEADER:
            # THE TAG HEADER FIRST CHAR MUST BE THE FIRST CHAR OF THE FILE !!! DO NOT ADD NEWLINE!!!
            result += tag[1]
        else:
            raise Error

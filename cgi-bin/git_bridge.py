#!/usr/bin/env python

import cgi
import cgitb
cgitb.enable()

import os
import json

from subprocess import Popen, PIPE

from xmlSerializer import prettyPrintValidXML
from schema import *

def report(data):
    print "Content-type: application/json\n"
    print json.dumps(data)


data = cgi.FieldStorage()

user = os.environ.get('ADFS_LOGIN', 'unknown')

XML_OUTPUT_PATH = "/tmp/tmp_pysvn_lhcb_nightlies_%s" % user
SCHEMA_FILE = os.path.join(XML_OUTPUT_PATH, 'configuration.xsd')
XML_FILE = os.path.join(XML_OUTPUT_PATH, 'configuration.xml')

GIT_REPO_URL = 'ssh://git@gitlab.cern.ch:7999/lhcb-core/LHCbNightlyConf.git'
GIT_BRANCH = 'master'

CLEAN_COMMAND='rm -r --force '+XML_OUTPUT_PATH
BUILD_COMMAND='mkdir '+XML_OUTPUT_PATH

GIT_CMD = '/afs/cern.ch/user/l/lhcbsoft/.local/git/bin/git'

os.environ['GIT_SSH'] = os.path.join(os.path.dirname(__file__), 'ssh-wrapper')

class GitError(RuntimeError):
    def __init__(self, *args, **kwargs):
        RuntimeError.__init__(self, *args, **kwargs)
        self.message = args[1]


def git(cmd, *args, **kwargs):
    if not kwargs.get('cwd'):
        kwargs['cwd'] = XML_OUTPUT_PATH
    for param in ('stdout', 'stderr'):
        if not param in kwargs:
            kwargs[param] = PIPE
    proc = Popen([GIT_CMD] + cmd, *args, **kwargs)
    out, err = proc.communicate()
    if proc.returncode:
        raise GitError(proc.returncode, err)
    return out


if data['action'].value=='checkout':
    # CLEAN UP THE DIR
    os.system(CLEAN_COMMAND)
    os.system(BUILD_COMMAND)
    git(['clone', '-b', GIT_BRANCH, GIT_REPO_URL, XML_OUTPUT_PATH])
    git(['config', 'user.name', 'web editor'])
    git(['config', 'user.email', 'noreply@cern.ch'])
    report(git(['rev-parse', 'HEAD']).strip())

elif data['action'].value=='commit':
    try:
        git(['commit',
             '--author', '{name} <{email}>'
                .format(name=os.environ.get('ADFS_FULLNAME'),
                        email=os.environ.get('ADFS_EMAIL').lower()),
             '-a', '-m', data['message'].value])
        git(['push'])
        report(git(['rev-parse', 'HEAD']).strip())
    except GitError, x:
        #report('ERROR:' + x.message)
        report('ERROR')

elif data['action'].value=='load':
    schema=Schema(SCHEMA_FILE)
    element=schema.parse(XML_FILE)
    report(open(XML_FILE, 'r').read())

elif data['action'].value=='check':
    file = open(XML_FILE, 'w')
    file.write(prettyPrintValidXML(data['xml'].value))
    file.close()
    schema=Schema(SCHEMA_FILE)
    element=schema.parse(XML_FILE)
    report('SUCCESS')

elif data['action'].value=='prettyprint':
    indentedXML = prettyPrintValidXML(data['xml'].value)
    report(indentedXML)

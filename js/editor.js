/*
 * JS functions for the configuration editor of the nightlies.
 */

// REDEFINITION OF METHODS NOT SUPPORTED BY IE

function hasAttribute(element,attribute){
	try{
		return element.hasAttribute(attribute);
	}catch(ex){
		return element.getAttribute(attribute)!=null;
	}
}


function indexOf(array,element){
	try{
		return array.indexOf(element);
	}catch(ex){
		for(var i=0; i<array.length; i++){
         		if(array[i]==element){
            			return i;
         		}
      		}
      		return -1;
	}
}







/*
 * Checks if a string contains only whitespaces
 */
function empty(el) {
    for ( i = 0; i < el.length; i++ ) {
        if ( el.charAt(i) != " " ) {
            return false;
        }
    }
    return true;
}


/*
 * Loads xml string in a dom object
 */
function loadXMLString(txt){
	if (window.DOMParser){
  		parser=new DOMParser();
  		xmlDoc=parser.parseFromString(txt,"text/xml");
  	}else{
		// Internet Explorer
  		xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
  		xmlDoc.async="false";
  		xmlDoc.loadXML(txt);
  	}
	return xmlDoc;
}


/*
 * Converts dom object to an xml string
 */
function convertObjToString(xmlObj){
	if (window.DOMParser){
		var xmlString = (new XMLSerializer()).serializeToString(xmlObj);
		return xmlString;
	}else{
		// Internet Explorer
		return xmlObj.xml;
	}
}


var ajax_request;
var loaded;

/*
 * Abort checkout of repo is cgi is not responding
 */
function abortCheckout(){
	if (loaded) return;
	ajax_request.abort();
	alert('Checkout of repository failed.');
}


/*
 * Check out the repository
 */
function checkout(){
	loaded=false;
	ajax_request = $.ajax({
		type: 'post',
		url: 'cgi-bin/git_bridge.py',
		dataType: 'json',
		data:	{
			'action': 'checkout'
			},
		success: function(result) {
            $('#info').spin(false);
			if (result){
				revisionStamp=result;
				loaded=true;
				loadPage();
			}
		}
	});

	setTimeout('abortCheckout()',10000);
}


/*
 * Loads the xml file
 */
function loadXMLFile(){
	var txt="ERROR";
	$.ajax({
		type: 'post',
		url: 'cgi-bin/git_bridge.py',
		dataType: 'json',
		data:	{
			'action':'load'
			},
		success: function(result) {
			if (result){
				txt=result;
			}
		},
		async:   false
	});

	if (txt=="ERROR"){
		alert('Error while loading the file, it may be not conform to the schema, please fix it manually');
		return null;
	}else{
		return loadXMLString(txt);
	}
}



function checkAgainstSchema(text){
	var checked=null;

	$.ajax({
		type: 'post',
		url: 'cgi-bin/git_bridge.py',
		dataType: 'json',
		data:	{
			'action' : 'check',
			'xml' : text
			},
		success: function(result) {
			if (result){
				checked=result;
			}
		},
		async:   false
	});
	return (checked!=null);
}



/*
 * Indents an xml string
 */
function prettyPrint(text){
	var res;

	$.ajax({
		type: 'post',
		url: 'cgi-bin/git_bridge.py',
		dataType: 'json',
		data:	{
			'action' : 'prettyprint',
			'xml' : text
			},
		success: function(result) {
			if (result){
				res=result;
			}
		},
		async:   false
	});

	return res;
}



/*
 * Writes to a file the result
 */
function writeToFile(text,msg){
	if (checkAgainstSchema(text)){
		var res;
		$.ajax({
			type: 'post',
			url: 'cgi-bin/git_bridge.py',
			dataType: 'json',
			data:	{
				'action' : 'commit',
				'revision' : revisionStamp,
				'message' : msg
				},
			success: function(result) {
				if (result){
					res=result;
				}
			},
			async:   false
		});

		if (res){
			if (res=='ERROR'){
				alert('ERROR - Someone committed something else, commit was aborted. You can still see and copy/paste your xml by clicking on See XML at the top');
			}else{
				alert('SUCCESS - New configuration was committed successfully, revision number : '+res);
				revisionStamp=res;
			}
		}else{
			alert('ERROR - Commit to repository failed');
		}
	}else{
		alert('ERROR - Check against the schema failed');
	}
}



function askDeleteConfirmation(){
	return (confirm("Are you sure you want to delete ?"));
}




function SimpleTableList(doc,data_type,parent_id,map)
{
	this.document=doc; // remember the document's name to know where we can find the table
	this.TABLE=null; // table in the document
	this.DATA_TYPE=data_type; // which type of data will be present in the list
	this.PARENT_ID = parent_id; // parent element of all the elements in this list
	this.map = map; // map to get element ids (xml)


	/*
	 * Creates the THead and TBody and fills the default rows : header
	 */
	this.initialize = function(container){
		this.TABLE = this.document.createElement("table");
		container.appendChild(this.TABLE);

		this.TABLE.createTHead();
		var tblBody = this.document.createElement("tbody");
		this.TABLE.appendChild(tblBody);

		// Add title - 1st row
		var title = this.TABLE.tHead.insertRow(0);
		var cell = title.insertCell(0);
		cell.colSpan=this.DATA_TYPE.length-2;
		cell.className='subtitle';

		var txt = this.document.createTextNode(this.DATA_TYPE[0].toUpperCase());
		cell.appendChild(txt);
		title.appendChild(cell);

		// Add attributes - 2nd row
		var attributes = this.TABLE.tHead.insertRow(1);

		for (var i=2 ; i<this.DATA_TYPE.length ; i++){
			cell = attributes.insertCell(i-2);
			cell.className='attribute';
			txt = this.document.createTextNode(this.DATA_TYPE[i][0]);
			cell.appendChild(txt);
			attributes.appendChild(cell);
		}


		// fill with the elements that have the right tag
		for (var j=0; j<this.map[this.PARENT_ID].childNodes.length; j++){
			if (this.map[this.PARENT_ID].childNodes[j].nodeType==1 && this.map[this.PARENT_ID].childNodes[j].nodeName==this.DATA_TYPE[0]){
				this.add(this.map[this.PARENT_ID].childNodes[j]);
			}
		}
	}


	/*
 	* Appends to the end - internal method
 	*/
	this.add = function(newel)
	{
		// add it to the dic
		var id=this.map.length;
		this.map[id]=newel;

		var nextRow = this.TABLE.tBodies[0].rows.length;
		// add the row
		var row = this.TABLE.tBodies[0].insertRow(nextRow);

		for (var i=2; i<this.DATA_TYPE.length; i++){
			var celli = row.insertCell(i-2);
			if (hasAttribute(newel,this.DATA_TYPE[i][0])){
				if (newel.getAttribute(this.DATA_TYPE[i][0])!=''){
					var txt = this.document.createTextNode(newel.getAttribute(this.DATA_TYPE[i][0]));
					celli.className='value';
				}else{
					var txt = this.document.createTextNode('*TO FILL*');
					celli.className= 'value';
				}
			}else{
				var txt = this.document.createTextNode('-');
				celli.className= 'empty';
			}

			celli.ondblclick = new Function("evt", "editCell(this,false)");
			celli.ELEMENT_TYPE = this.DATA_TYPE[i];
			celli.ELEMENT_ID = id;
			celli.MAP = this.map;
			celli.fixedClass = 'value';
			celli.appendChild(txt);
		}
	}


}






/*
 *
 */
function TableList(doc,data_type,parent_list,parent_id,map,map_tablelist,map_tablelist_name,xmlObj)
{
	this.document=doc; // remember the document's name to know where we can find the table
	this.TABLE=null; // table in the document
	this.DATA_TYPE=data_type; // which type of data will be present in the list
	this.PARENT_LIST=parent_list; // if it is a subtable, we need to remember the other list to be able to call 'updateSeeChildren'
	this.PARENT_ID = parent_id; // parent element of all the elements in this list
	this.map = map; // map to get element ids (xml)
	this.MAP_TABLELIST_IDENTIFIER = map_tablelist_name;
	this.MAP_TABLELIST = map_tablelist;
	this.ID=-1;

	this.size=0;

	this.xmlObj = xmlObj; // the XML


	/*
	 * Creates the THead and TBody and fills the default rows : header + Add at the end of Tbody
	 */
	this.initialize = function(container){
		this.ID=this.MAP_TABLELIST.length;
		this.MAP_TABLELIST[this.ID]=this;
		this.TABLE = this.document.createElement("table");
		container.appendChild(this.TABLE);

		this.TABLE.createTHead();
		var tblBody = this.document.createElement("tbody");
		this.TABLE.appendChild(tblBody);

		// Add title - 1st row
		var title = this.TABLE.tHead.insertRow(0);
		var cell = title.insertCell(0);
		cell.colSpan=this.DATA_TYPE.length;
		cell.className='subtitle';


		var txt = this.document.createTextNode(this.DATA_TYPE[0].toUpperCase());
		cell.appendChild(txt);
		title.appendChild(cell);

		// Add attributes - 2nd row
		var attributes = this.TABLE.tHead.insertRow(1);
		cell = attributes.insertCell(0);
		cell.className='attribute';
		txt = this.document.createTextNode('-');
		cell.appendChild(txt);
		attributes.appendChild(cell);

		for (var i=2 ; i<this.DATA_TYPE.length ; i++){
			cell = attributes.insertCell(i-1);
			cell.className='attribute';
			txt = this.document.createTextNode(this.DATA_TYPE[i][0]);
			cell.appendChild(txt);
			attributes.appendChild(cell);
		}

		cell = attributes.insertCell(this.DATA_TYPE.length-1);
		cell.className='attribute';
		txt = this.document.createTextNode('-');
		cell.appendChild(txt);
		attributes.appendChild(cell);

		// Add 'add' button - Last row in tbody
		var add = this.TABLE.tBodies[0].insertRow(0);
		cell = add.insertCell(0);
		cell.className='buttoncell';
		cell.colSpan=this.DATA_TYPE.length;
		cell.onclick = new Function("evt",this.MAP_TABLELIST_IDENTIFIER+"["+String(this.ID)+"].addNew()");
		txt = this.document.createTextNode('Add');
		cell.appendChild(txt);
		add.appendChild(cell);


		// fill with the elements that have the right tag
		for (var j=0; j<this.map[this.PARENT_ID].childNodes.length; j++){
			if (this.map[this.PARENT_ID].childNodes[j].nodeType==1 && this.map[this.PARENT_ID].childNodes[j].nodeName==this.DATA_TYPE[0]){
				this.add(this.map[this.PARENT_ID].childNodes[j],-1);
			}
		}
	}


	/*
 	* Appends to the end or inserts if index given
 	*/
	this.add = function(newel,index)
	{
		// add it to the dic
		var id=this.map.length;
		this.map[id]=newel;

		if (index==-1){
			var nextRow = this.TABLE.tBodies[0].rows.length-1;
		}else{
			var nextRow = index;
		}
		// add the row
		var row = this.TABLE.tBodies[0].insertRow(nextRow);


		// cell 0 - insert
		var cell0 = row.insertCell(0);
		var raEl = this.document.createElement('img');
		raEl.setAttribute('src', 'images/insert.png');
		raEl.setAttribute('title', 'Insert');
		raEl.setAttribute('height', '20');
		raEl.setAttribute('width', '20');
		cell0.className='buttoncell';
		cell0.onclick = new Function("evt", this.MAP_TABLELIST_IDENTIFIER+"["+String(this.ID)+"]"+".insertNew(this.parentNode)");
		cell0.appendChild(raEl);

		for (var i=2; i<this.DATA_TYPE.length; i++){
			var celli = row.insertCell(i-1);
			if (hasAttribute(newel,this.DATA_TYPE[i][0])){
				if (newel.getAttribute(this.DATA_TYPE[i][0])!=''){
					var txt = this.document.createTextNode(newel.getAttribute(this.DATA_TYPE[i][0]));
					celli.className='value';
				}else{
					var txt = this.document.createTextNode('*TO FILL*');
					celli.className='value';
				}
			}else{
				var txt = this.document.createTextNode('-');
				celli.className='empty';
			}

			celli.ondblclick = new Function("evt", "editCell(this,false)");
			celli.ELEMENT_TYPE = this.DATA_TYPE[i];
			celli.ELEMENT_ID = id;
			celli.MAP = this.map;
			celli.fixedClass = 'value';
			celli.appendChild(txt);
		}

		// last cell - delete
		var cellX = row.insertCell(this.DATA_TYPE.length-1);
		var inEl = this.document.createElement('img');
		inEl.setAttribute('src', 'images/delete.png');
		inEl.setAttribute('title', 'Delete');
		inEl.setAttribute('height', '20');
		inEl.setAttribute('width', '20');
		cellX.className='buttoncell';
		cellX.onclick = new Function('evt',this.MAP_TABLELIST_IDENTIFIER+'['+String(this.ID)+']'+'.deleteElem('+String(id)+')');
		cellX.appendChild(inEl);

		row.ELEMENT_ID=id;
		this.size++;
		if (this.PARENT_LIST!=null){
			this.PARENT_LIST.updateSeeChildren(this.PARENT_ID);
		}
	}


	this.insertNew = function(row){
		// try to create the new element in the tree and check in cgi
		newel=this.xmlObj.createElement(this.DATA_TYPE[0]);
		for (var i=2; i<this.DATA_TYPE.length; i++){
			if (this.DATA_TYPE[i][2].length>0){
				// default value
				newel.setAttribute(this.DATA_TYPE[i][0],this.DATA_TYPE[i][2][0]);
			}else if (this.DATA_TYPE[i][1]){
				// attribute is required, initialize it with a dummy string
				newel.setAttribute(this.DATA_TYPE[i][0],'*TO FILL*');
			}
		}

		var nextNode=this.map[this.TABLE.tBodies[0].rows[row.sectionRowIndex].ELEMENT_ID]
		this.map[this.PARENT_ID].insertBefore(newel,nextNode);

		// add it to the table
		this.add(newel,row.sectionRowIndex);
	}


	this.addNew = function(){
		// try to create the new element in the tree and check in cgi
		newel=this.xmlObj.createElement(this.DATA_TYPE[0]);
		for (var i=2; i<this.DATA_TYPE.length; i++){
			if (this.DATA_TYPE[i][2].length>0){
				// default value
				newel.setAttribute(this.DATA_TYPE[i][0],this.DATA_TYPE[i][2][0]);
			}else if (this.DATA_TYPE[i][1]){
				// attribute is required, initialize it with a dummy string
				newel.setAttribute(this.DATA_TYPE[i][0],'*TO FILL*');
			}
		}
		if (this.DATA_TYPE[1].length<=1){
			this.map[this.PARENT_ID].appendChild(newel);
		}else{
			var sequenceIndex = indexOf(this.DATA_TYPE[1],this.DATA_TYPE[0]);
			var nextTags=new Array();
			for (var i=sequenceIndex+1;i<this.DATA_TYPE[1].length; i++){
				nextTags[nextTags.length]=this.DATA_TYPE[1][i];
			}
			if (nextTags.length==0){
				// Last position in sequence : we can appendChild
				this.map[this.PARENT_ID].appendChild(newel);
			}else{
				// If we find a tag that is in nextTags, we insert before it, else we appendChild
				for (var i=0; i<this.map[this.PARENT_ID].childNodes.length; i++){
					if (	this.map[this.PARENT_ID].childNodes[i].nodeType==1 &&
						indexOf(nextTags,this.map[this.PARENT_ID].childNodes[i].nodeName)!=-1)
						{
							this.map[this.PARENT_ID].insertBefore(newel,this.map[this.PARENT_ID].childNodes[i]);
							this.add(newel,-1);
							return;
						}
				}
				// Did not find a tag which should be located after the newel, so we can appendChild
				this.map[this.PARENT_ID].appendChild(newel);
			}

		}

		// add it to the table
		this.add(newel,-1);
	}

	this.deleteElem = function(id){
		if (!askDeleteConfirmation()) return;
		//try to remove the element from the tree and check in cgi
		this.map[id].parentNode.removeChild(this.map[id]);

		// remove it from the table
		for (var i=0; i<this.TABLE.tBodies[0].rows.length; i++) {
			if (this.TABLE.tBodies[0].rows[i].ELEMENT_ID==id) {
				this.TABLE.tBodies[0].rows[i].parentNode.deleteRow(this.TABLE.tBodies[0].rows[i].sectionRowIndex);
				break;
			}
		}
		this.size--;
		if (this.PARENT_LIST!=null){
			this.PARENT_LIST.updateSeeChildren(this.PARENT_ID);
		}
	}


}









/*
 *
 */
function DoubleTableList(doc,data_type,children_types,parent_id,map,map_tablelist,map_tablelist_name,xmlObj)
{
	this.document=doc; // remember the document's name to know where we can find the table
	this.TABLE=null; // table in the document
	this.DATA_TYPE=data_type // which type of data will be present in the list
	this.CHILDREN_TYPES=children_types; // sub_element data types
	this.PARENT_ID = parent_id; // parent element of all the elements in this list
	this.map = map; // map to get element ids
	this.MAP_TABLELIST_IDENTIFIER = map_tablelist_name;
	this.MAP_TABLELIST = map_tablelist;
	this.ID=-1; // ID in the map_tablelist dictionary
	this.xmlObj = xmlObj; // the XML
	this.size=0;

	/*
	 * Creates the THead and TBody and fills the default rows : header + Add at the end of Tbody
	 */
	this.initialize = function(container){
		this.ID=this.MAP_TABLELIST.length;
		this.MAP_TABLELIST[this.ID]=this;
		this.TABLE = this.document.createElement("table");
		//container.appendChild(this.TABLE);
		if (container.childNodes.length>0)
			container.removeChild(container.childNodes[0]);
		container.appendChild(this.TABLE);


		this.TABLE.createTHead();
		var tblBody = this.document.createElement("tbody");
		this.TABLE.appendChild(tblBody);

		// Add title - 1st row
		var title = this.TABLE.tHead.insertRow(0);
		var cell = title.insertCell(0);
		cell.colSpan = this.DATA_TYPE.length+1;
		cell.className='title';


		var txt = this.document.createTextNode(this.DATA_TYPE[0].toUpperCase());
		cell.appendChild(txt);
		title.appendChild(cell);

		// Add attributes - 2nd row
		var attributes = this.TABLE.tHead.insertRow(1);
		cell = attributes.insertCell(0);
		cell.className='attribute';
		txt = this.document.createTextNode('-');
		cell.appendChild(txt);
		attributes.appendChild(cell);

		cell = attributes.insertCell(1);
		cell.className='attribute';
		txt = this.document.createTextNode('-');
		cell.appendChild(txt);
		attributes.appendChild(cell);

		for (var i=2 ; i<this.DATA_TYPE.length ; i++){
			cell = attributes.insertCell(i);
			cell.className='attribute';
			txt = this.document.createTextNode(this.DATA_TYPE[i][0]);
			cell.appendChild(txt);
			attributes.appendChild(cell);
		}


		cell = attributes.insertCell(this.DATA_TYPE.length);
		cell.className='attribute';
		txt = this.document.createTextNode('-');
		cell.appendChild(txt);
		attributes.appendChild(cell);

		// Add 'add' button - Last row in tbody
		var add = this.TABLE.tBodies[0].insertRow(0);
		cell = add.insertCell(0);
		cell.className='buttoncell';
		cell.colSpan = this.DATA_TYPE.length+1;
		cell.onclick = new Function("evt",this.MAP_TABLELIST_IDENTIFIER+"["+String(this.ID)+"].addNew()");
		txt = this.document.createTextNode('Add');
		cell.appendChild(txt);
		add.appendChild(cell);

		// fill with the elements that have the right tag
		for (var j=0 ;j<this.map[this.PARENT_ID].childNodes.length; j++){
			if (this.map[this.PARENT_ID].childNodes[j].nodeType==1 && this.map[this.PARENT_ID].childNodes[j].nodeName==this.DATA_TYPE[0]){
				this.add(this.map[this.PARENT_ID].childNodes[j],-1);
			}
		}
	}


	/*
 	* Appends to the end or inserts if index given
 	*/
	this.add = function(newel,index)
	{
		// add it to the dic
		var id=this.map.length;
		this.map[id]=newel;

		if (index==-1){
			var nextRow = this.TABLE.tBodies[0].rows.length-1;
		}else{
			var nextRow = index;
		}
		// add the row
		var row = this.TABLE.tBodies[0].insertRow(nextRow);


		// cell 0 - insert
		var cell0 = row.insertCell(0);
		var raEl = this.document.createElement('img');
		raEl.setAttribute('src', 'images/insert.png');
		raEl.setAttribute('title', 'Insert');
		raEl.setAttribute('height', '25');
		raEl.setAttribute('width', '25');
		cell0.className='buttoncell';
		cell0.onclick = new Function("evt", this.MAP_TABLELIST_IDENTIFIER+"["+String(this.ID)+"]"+".insertNew(this.parentNode)");
		cell0.appendChild(raEl);

		// cell1 - see children
		var cell1 = row.insertCell(1);
		raEl = this.document.createElement('img');
		raEl.setAttribute('src', 'images/expand.png');
		raEl.setAttribute('title', 'See children');
		raEl.setAttribute('height', '20');
		raEl.setAttribute('width', '20');
		cell1.className='buttoncell';
		cell1.onclick = new Function("evt", this.MAP_TABLELIST_IDENTIFIER+"["+String(this.ID)+"].changeVisibility("+String(id)+")");
		cell1.appendChild(raEl);

		for (var i=2; i<this.DATA_TYPE.length; i++){
			var celli = row.insertCell(i);
			if (hasAttribute(newel,this.DATA_TYPE[i][0])){
				if (newel.getAttribute(this.DATA_TYPE[i][0])!=''){
					var txt = this.document.createTextNode(newel.getAttribute(this.DATA_TYPE[i][0]));
					celli.className='doubletable_item';
				}else{
					var txt = this.document.createTextNode('*TO FILL*');
					celli.className='doubletable_item';
				}
			}else{
				var txt = this.document.createTextNode('-');
				celli.className='empty';
			}

			celli.ondblclick = new Function("evt", "editCell(this,false)");
			celli.ELEMENT_TYPE = this.DATA_TYPE[i];
			celli.ELEMENT_ID = id;
			celli.MAP = this.map;
			celli.fixedClass = 'doubletable_item';
			celli.appendChild(txt);
		}

		// last cell - delete
		var cellX = row.insertCell(this.DATA_TYPE.length);
		var inEl = this.document.createElement('img');
		inEl.setAttribute('src', 'images/delete.png');
		inEl.setAttribute('title', 'Delete');
		inEl.setAttribute('height', '25');
		inEl.setAttribute('width', '25');
		cellX.onclick = new Function('evt',this.MAP_TABLELIST_IDENTIFIER+'['+String(this.ID)+']'+'.deleteElem('+String(id)+')');
		cellX.className='buttoncell';
		cellX.appendChild(inEl);

		row.ELEMENT_ID=id;
		row.SUBLISTS=new Array();

		for (var i=0; i<this.CHILDREN_TYPES.length; i++){
			nextRow +=1;
			var childRow = this.TABLE.tBodies[0].insertRow(nextRow);

			cell = childRow.insertCell(0);
			cell.className='empty';
			txt = this.document.createTextNode('-');
			cell.appendChild(txt);
			childRow.appendChild(cell);


			cell = childRow.insertCell(1);
			cell.colSpan=this.DATA_TYPE.length-1;
			var tbl=new TableList(this.document,this.CHILDREN_TYPES[i],this,id,this.map,this.MAP_TABLELIST,this.MAP_TABLELIST_IDENTIFIER,this.xmlObj);
			tbl.initialize(cell);
			tbl.TABLE.className='subtable';
			row.SUBLISTS[row.SUBLISTS.length]=tbl;
			childRow.appendChild(cell);


			cell = childRow.insertCell(2);
			cell.className='empty';
			txt = this.document.createTextNode('-');
			cell.appendChild(txt);
			childRow.appendChild(cell);
		}

		// Hide children by default
		for (var l=0; l<this.CHILDREN_TYPES.length; l++){
			this.TABLE.tBodies[0].rows[row.sectionRowIndex+1+l].style.display="none";
		}
		this.updateSeeChildren(id,row);
		this.size++;
	}


	this.insertNew = function(row){
		// try to create the new element in the tree and check in cgi
		newel=this.xmlObj.createElement(this.DATA_TYPE[0]);
		for (var i=2; i<this.DATA_TYPE.length; i++){
			if (this.DATA_TYPE[i][2].length>0){
				// default value
				newel.setAttribute(this.DATA_TYPE[i][0],this.DATA_TYPE[i][2][0]);
			}else if (this.DATA_TYPE[i][1]){
				// attribute is required, initialize it with a dummy string
				newel.setAttribute(this.DATA_TYPE[i][0],'*TO FILL*');
			}
		}

		nextNode=this.map[this.TABLE.tBodies[0].rows[row.sectionRowIndex].ELEMENT_ID]
		this.map[this.PARENT_ID].insertBefore(newel,nextNode);

		// add it to the table
		this.add(newel,row.sectionRowIndex);
	}


	this.addNew = function(){
		// try to create the new element in the tree and check in cgi
		newel=this.xmlObj.createElement(this.DATA_TYPE[0]);
		for (var i=2; i<this.DATA_TYPE.length; i++){
			if (this.DATA_TYPE[i][2].length>0){
				// default value
				newel.setAttribute(this.DATA_TYPE[i][0],this.DATA_TYPE[i][2][0]);
			}else if (this.DATA_TYPE[i][1]){
				// attribute is required, initialize it with a dummy string
				newel.setAttribute(this.DATA_TYPE[i][0],'*TO FILL*');
			}
		}
		if (this.DATA_TYPE[1].length<=1){
			this.map[this.PARENT_ID].appendChild(newel);
		}else{
			var sequenceIndex = indexOf(this.DATA_TYPE[1],this.DATA_TYPE[0]);
			var nextTags=new Array();
			for (var i=sequenceIndex+1;i<this.DATA_TYPE[1].length; i++){
				nextTags[nextTags.length]=this.DATA_TYPE[1][i];
			}
			if (nextTags.length==0){
				// Last position in sequence : we can appendChild
				this.map[this.PARENT_ID].appendChild(newel);
			}else{
				// If we find a tag that is in nextTags, we insert before it, else we appendChild
				for (var i=0; i<this.map[this.PARENT_ID].childNodes.length; i++){
					if (	this.map[this.PARENT_ID].childNodes[i].nodeType==1 &&
						indexOf(nextTags,this.map[this.PARENT_ID].childNodes[i].nodeName)!=-1)
						{
							this.map[this.PARENT_ID].insertBefore(newel,this.map[this.PARENT_ID].childNodes[i]);
							this.add(newel,-1);
							return;
						}
				}
				// Did not find a tag which should be located after the newel, so we can appendChild
				this.map[this.PARENT_ID].appendChild(newel);
			}

		}

		// add it to the table
		this.add(newel,-1);
	}

	this.deleteElem = function(id)
	{
		if (!askDeleteConfirmation()) return;
		//try to remove the element from the tree and check in cgi
		this.map[id].parentNode.removeChild(this.map[id]);

		// remove it from the table, and remove the child rows
		for (var i=0; i<this.TABLE.tBodies[0].rows.length; i++) {
			if (this.TABLE.tBodies[0].rows[i].ELEMENT_ID && this.TABLE.tBodies[0].rows[i].ELEMENT_ID==id) {
				for (var k=0; k<this.CHILDREN_TYPES.length; k++){
					this.TABLE.tBodies[0].rows[i].parentNode.deleteRow(this.TABLE.tBodies[0].rows[i].sectionRowIndex+1);
				}
				this.TABLE.tBodies[0].rows[i].parentNode.deleteRow(this.TABLE.tBodies[0].rows[i].sectionRowIndex);
				break;
			}
		}
		this.size--;
	}

	this.updateSeeChildren = function(id,row){
		var r=null;
		if (row==null){
			for (var i=0; i<this.TABLE.tBodies[0].rows.length; i++) {
				if (this.TABLE.tBodies[0].rows[i].ELEMENT_ID && this.TABLE.tBodies[0].rows[i].ELEMENT_ID==id) {
					r=this.TABLE.tBodies[0].rows[i];
					break;
				}
			}
		}else{
			r=row;
		}
		var image=r.childNodes[1].childNodes[0];
		for (var i=0; i<r.SUBLISTS.length; i++){
			if (r.SUBLISTS[i].size>0){
				image.setAttribute('src','images/expand.png');
				image.setAttribute('title','See children');
				return true;
			}
		}
		image.setAttribute('src','images/expand_empty.png');
		image.setAttribute('title','Add children');
		return false;
	}


	this.changeVisibility = function(id){
		for (var i=0; i<this.TABLE.tBodies[0].rows.length; i++) {
			if (this.TABLE.tBodies[0].rows[i].ELEMENT_ID && this.TABLE.tBodies[0].rows[i].ELEMENT_ID==id) {
				if (this.TABLE.tBodies[0].rows[this.TABLE.tBodies[0].rows[i].sectionRowIndex+1].style.display=="none"){
					for (var l=0; l<this.CHILDREN_TYPES.length; l++){
						this.TABLE.tBodies[0].rows[this.TABLE.tBodies[0].rows[i].sectionRowIndex+1+l].style.display="";
					}
				}else{
					for (var l=0; l<this.CHILDREN_TYPES.length; l++){
						this.TABLE.tBodies[0].rows[this.TABLE.tBodies[0].rows[i].sectionRowIndex+1+l].style.display="none";
					}
				}
			}
		}
	}
}






//<!--   ProjectTableList(document,SLOT_PROJECT_TYPE,[CHANGE_TYPE],DEPENDENCE_TYPE,map.length-1,map_ID,map_TABLELIST,'map_TABLELIST',xmlObj);-->
function ProjectTableList(doc,     data_type,        children_types,column_child_type,parent_id,map,map_tablelist,map_tablelist_name,xmlObj)
{
	this.document=doc; // remember the document's name to know where we can find the table
	this.TABLE=null; // table in the document
	this.DATA_TYPE=data_type // which type of data will be present in the list
	this.CHILDREN_TYPES=children_types; // sub_element data types
	this.COLUMN_CHILD_TYPE=column_child_type; // type of the elements which will be put on the right (dependences)
	this.PARENT_ID = parent_id; // parent element of all the elements in this list
	this.map = map; // map to get element ids
	this.MAP_TABLELIST_IDENTIFIER = map_tablelist_name;
	this.MAP_TABLELIST = map_tablelist;
	this.ID=-1; // ID in the map_tablelist dictionary
	this.xmlObj = xmlObj; // the XML
	this.size=0;
	this.COLUMN_ELEMENTS = new Array();

	/*
	 * Creates the THead and TBody and fills the default rows : header + Add at the end of Tbody
	 */
	this.initialize = function(container){
		this.ID=this.MAP_TABLELIST.length;
		this.MAP_TABLELIST[this.ID]=this;
		this.TABLE = this.document.createElement("table");
		//container.appendChild(this.TABLE);
		if (container.childNodes.length>0)
			container.removeChild(container.childNodes[0]);
		container.appendChild(this.TABLE);


		this.TABLE.createTHead();
		var tblBody = this.document.createElement("tbody");
		this.TABLE.appendChild(tblBody);

		// Add title - 1st row
		var title = this.TABLE.tHead.insertRow(0);
		var cell = title.insertCell(0);
		cell.colSpan = this.DATA_TYPE.length+1;
		cell.className='title';
		var txt = this.document.createTextNode(this.DATA_TYPE[0].toUpperCase());
		cell.appendChild(txt);
		title.appendChild(cell);


		// Add attributes - 2nd row
		var attributes = this.TABLE.tHead.insertRow(1);
		cell = attributes.insertCell(0);
		cell.className='attribute';
		txt = this.document.createTextNode('-');
		cell.appendChild(txt);
		attributes.appendChild(cell);

		cell = attributes.insertCell(1);
		cell.className='attribute';
		txt = this.document.createTextNode('-');
		cell.appendChild(txt);
		attributes.appendChild(cell);

		for (var i=2 ; i<this.DATA_TYPE.length ; i++){
			cell = attributes.insertCell(i);
			cell.className='attribute';
			txt = this.document.createTextNode(this.DATA_TYPE[i][0]);
			cell.appendChild(txt);
			attributes.appendChild(cell);
		}


		cell = attributes.insertCell(this.DATA_TYPE.length);
		cell.className='attribute';
		txt = this.document.createTextNode('-');
		cell.appendChild(txt);
		attributes.appendChild(cell);

		// Add 'add' button - Last row in tbody
		var add = this.TABLE.tBodies[0].insertRow(0);
		cell = add.insertCell(0);
		cell.className='buttoncell';
		cell.colSpan = this.DATA_TYPE.length;
		cell.onclick = new Function("evt",this.MAP_TABLELIST_IDENTIFIER+"["+String(this.ID)+"].addNew()");
		txt = this.document.createTextNode('Add');
		cell.appendChild(txt);
		add.appendChild(cell);
		// Add column button - Last row last cell
		cell = add.insertCell(1);
		cell.className='buttoncell';
		//cell.onclick = new Function("evt",this.MAP_TABLELIST_IDENTIFIER+"["+String(this.ID)+"].addColumnChild(['Gaudi','v21r2'])");
		cell.onclick = new Function("evt","dependence="+String(this.ID)+"; $('#dependence_div').dialog('open')");
		txt = this.document.createTextNode('Add ' + this.COLUMN_CHILD_TYPE[0]);
		cell.appendChild(txt);
		add.appendChild(cell);

		// fill with the elements that have the right tag
		for (var j=0; j<this.map[this.PARENT_ID].childNodes.length; j++){
			if (this.map[this.PARENT_ID].childNodes[j].nodeType==1 && this.map[this.PARENT_ID].childNodes[j].nodeName==this.DATA_TYPE[0]){
				this.add(this.map[this.PARENT_ID].childNodes[j],-1);
			}
		}
	}


	this.hasColumnChild = function(child_attribute_values){
		if (this.COLUMN_ELEMENTS.length>0){
			for (var i=0;i<this.COLUMN_ELEMENTS.length; i++){
				var childAlreadyExists = true;
				for (var j=0; j<this.COLUMN_CHILD_TYPE.length-2; j++){
					if (this.COLUMN_ELEMENTS[i][j]!=child_attribute_values[j]){
						childAlreadyExists=false;
						break;
					}
				}
				if (childAlreadyExists) return true;
			}
		}
		return false;
	}

	this.getColumnChild = function(child_attribute_values){
		if (this.COLUMN_ELEMENTS.length>0){
			for (var i=0;i<this.COLUMN_ELEMENTS.length; i++){
				var childAlreadyExists = true;
				for (var j=0; j<this.COLUMN_CHILD_TYPE.length-2; j++){
					if (this.COLUMN_ELEMENTS[i][j]!=child_attribute_values[j]){
						childAlreadyExists=false;
						break;
					}
				}
				if (childAlreadyExists) return i;
			}
		}
		return -1;
	}


	this.addColumnChild = function(child_attribute_values){
		// Check that the child does not already exist in the columns

		if (this.COLUMN_CHILD_TYPE.length-2!=child_attribute_values.length || child_attribute_values.length==0){
			alert('The column cannot be added, the number of attributes of the column must be wrong.');
			return;
		}

		if (this.hasColumnChild(child_attribute_values)){
			return;
		}

		this.COLUMN_ELEMENTS[this.COLUMN_ELEMENTS.length]=child_attribute_values;

		// Add attributes of the new column
		var cell;
		var txt;
		for (var l=0; l<child_attribute_values.length; l++){
			cell = this.TABLE.tHead.rows[1].insertCell(this.TABLE.tHead.rows[1].cells.length);
			cell.className='attribute';
			cell.ondblclick = new Function("evt", "editCell(this,true)");
			txt = this.document.createTextNode(child_attribute_values[l]);
			cell.appendChild(txt);
			this.TABLE.tHead.rows[1].appendChild(cell);

			cell.LIST = this;
			cell.WHOLE_TYPE = this.COLUMN_CHILD_TYPE;
			cell.ELEMENT_TYPE = this.COLUMN_CHILD_TYPE[l+2];
			cell.ATTRIBUTES = child_attribute_values;
			cell.AT_INDEX = l;
			cell.TBODY = this.TABLE.tBodies[0];
			cell.MAP = this.map;
		}

		// 'Delete' column child
		cell = this.TABLE.tHead.rows[1].insertCell(this.TABLE.tHead.rows[1].cells.length);
		cell.className='buttoncell';
		cell.onclick = new Function("evt", this.MAP_TABLELIST_IDENTIFIER+"["+String(this.ID)+"].removeColumnChild(this)");
		var inEl = this.document.createElement('img');
		inEl.setAttribute('src', 'images/delete.png');
		inEl.setAttribute('title', 'Delete');
		inEl.setAttribute('height', '15');
		inEl.setAttribute('width', '15');
		cell.ATTRIBUTES = child_attribute_values;
		cell.appendChild(inEl);



		// Add a cell for each content row in the table
		for (var i=0; i<this.TABLE.tBodies[0].rows.length-1; i++){
			cell = this.TABLE.tBodies[0].rows[i].insertCell(this.TABLE.tBodies[0].rows[i].cells.length);
			cell.className='attribute';
			cell.colSpan=this.COLUMN_CHILD_TYPE.length-1;
			var check = this.document.createElement('input');
			check.setAttribute('type','checkbox');
			check.onclick = new Function("evt", this.MAP_TABLELIST_IDENTIFIER+"["+String(this.ID)+"]"+".onclickColumnChildCell(this.parentNode)");
			cell.appendChild(check);
			cell.ATTRIBUTES = child_attribute_values;
			this.TABLE.tBodies[0].rows[i].appendChild(cell);

			for (var l=0; l<this.CHILDREN_TYPES.length; l++){
				// change colspan of the last cell in children rows
				this.TABLE.tBodies[0].rows[i+l+1].cells[this.TABLE.tBodies[0].rows[i+l+1].cells.length-1].colSpan=this.COLUMN_ELEMENTS.length*(this.COLUMN_CHILD_TYPE.length-1)+1;
				i++;
			}
		}


		if (this.COLUMN_ELEMENTS.length==1){
			// Add cell in the header
			cell = this.TABLE.tHead.rows[0].insertCell(1);
			cell.className='title';
			cell.colSpan=this.COLUMN_CHILD_TYPE.length-1;
			txt = this.document.createTextNode(this.COLUMN_CHILD_TYPE[0].toUpperCase());
			cell.appendChild(txt);
			this.TABLE.tHead.rows[0].appendChild(cell);

			// Change the colspan of the 'Add' cells
			this.TABLE.tBodies[0].rows[this.TABLE.tBodies[0].rows.length-1].cells[0].colSpan=this.DATA_TYPE.length+1;
			this.TABLE.tBodies[0].rows[this.TABLE.tBodies[0].rows.length-1].cells[1].colSpan=this.COLUMN_CHILD_TYPE.length-1;
		}else{
			// Change cell colspan in the header
			this.TABLE.tHead.rows[0].cells[1].colSpan=this.COLUMN_ELEMENTS.length*(this.COLUMN_CHILD_TYPE.length-1);

			// Change the colspan of the 'Add column child'
			this.TABLE.tBodies[0].rows[this.TABLE.tBodies[0].rows.length-1].cells[1].colSpan=this.COLUMN_ELEMENTS.length*(this.COLUMN_CHILD_TYPE.length-1);
		}


	}


	this.removeColumnChild = function(cell){
		if (!askDeleteConfirmation()) return;
		var index = this.getColumnChild(cell.ATTRIBUTES);
		var at_values = this.COLUMN_ELEMENTS[index];
		var checkbox_index = this.DATA_TYPE.length+1+index;
		var at_index = this.DATA_TYPE.length+1+(this.COLUMN_CHILD_TYPE.length-1)*index;


		// remove all the children of the checked (checkbox) row elements having the same attributes than the column child
		var tbody=this.TABLE.tBodies[0];
		for (var row=0; row<tbody.rows.length ; row++){
			if (tbody.rows[row].ELEMENT_ID && tbody.rows[row].cells[checkbox_index].childNodes[0].checked){
				//cell is checked, we need to remove the element
				var element = this.map[tbody.rows[row].ELEMENT_ID];
				for (var child=0; child<element.childNodes.length; child++){
					if (element.childNodes[child].nodeType==1 && element.childNodes[child].nodeName==this.COLUMN_CHILD_TYPE[0]){
						var toRemove = true;
						for (var at=0; at<at_values.length; at++){
							if (element.childNodes[child].getAttribute(this.COLUMN_CHILD_TYPE[at+2][0])!=at_values[at]){
								toRemove=false;
								break;
							}
						}
						if (toRemove){
							element.removeChild(element.childNodes[child]);
						}
					}
				}
			}
		}

		// remove the element from the array
		this.COLUMN_ELEMENTS.splice(index,1);

		// remove the column from the table
		// HEADER + 'ADD' CELLS
		if (this.COLUMN_ELEMENTS.length==0){
			// Remove cell in the header
			this.TABLE.tHead.rows[0].removeChild(this.TABLE.tHead.rows[0].cells[this.TABLE.tHead.rows[0].cells.length-1]);

			// 'Add cells'
			this.TABLE.tBodies[0].rows[this.TABLE.tBodies[0].rows.length-1].cells[0].colSpan=this.DATA_TYPE.length;
			this.TABLE.tBodies[0].rows[this.TABLE.tBodies[0].rows.length-1].cells[1].colSpan=1;
		}else{
			// Change cell colspan in the header
			this.TABLE.tHead.rows[0].cells[1].colSpan=this.COLUMN_ELEMENTS.length*(this.COLUMN_CHILD_TYPE.length-1);

			// Change the colspan of the 'Add column child'
			this.TABLE.tBodies[0].rows[this.TABLE.tBodies[0].rows.length-1].cells[1].colSpan=this.COLUMN_ELEMENTS.length*(this.COLUMN_CHILD_TYPE.length-1);
		}

		// ATTRIBUTES CELLS
		for (var l=0; l<this.COLUMN_CHILD_TYPE.length-1; l++){
			this.TABLE.tHead.rows[1].removeChild(this.TABLE.tHead.rows[1].cells[at_index]);
		}

		// CONTENT CELLS
		for (var i=0; i<this.TABLE.tBodies[0].rows.length-1; i++){
			this.TABLE.tBodies[0].rows[i].removeChild(this.TABLE.tBodies[0].rows[i].cells[checkbox_index]);
			for (var l=0; l<this.CHILDREN_TYPES.length; l++){
				// change colspan of the last cell in children rows
				this.TABLE.tBodies[0].rows[i+l+1].cells[this.TABLE.tBodies[0].rows[i+l+1].cells.length-1].colSpan=this.COLUMN_ELEMENTS.length*(this.COLUMN_CHILD_TYPE.length-1)+1;
				i++;
			}
		}

	}



	this.onclickColumnChildCell = function(cell){
		var checkbox = cell.childNodes[0];
		var attributes = cell.ATTRIBUTES;
		var parent = this.map[cell.parentNode.ELEMENT_ID];

		if (checkbox.checked){
			// add
			var newel=this.xmlObj.createElement(this.COLUMN_CHILD_TYPE[0]);
			for (var i=2; i<this.COLUMN_CHILD_TYPE.length; i++){
				newel.setAttribute(this.COLUMN_CHILD_TYPE[i][0],attributes[i-2]);
			}
			if (this.DATA_TYPE[1].length<=1){
				parent.appendChild(newel);
			}else{
				var sequenceIndex = indexOf(this.COLUMN_CHILD_TYPE[1],this.COLUMN_CHILD_TYPE[0]);
				var nextTags=new Array();
				for (var i=sequenceIndex+1;i<this.COLUMN_CHILD_TYPE.length; i++){
					nextTags[nextTags.length]=this.COLUMN_CHILD_TYPE[1][i];
				}
				if (nextTags.length==0){
					// Last position in sequence : we can appendChild
					parent.appendChild(newel);
				}else{
					// If we find a tag that is in nextTags, we insert before it, else we appendChild
					for (var i=0; i<element.childNodes.length; i++){
						if (	element.childNodes[i].nodeType==1 &&
							indexOf(nextTags,element.childNodes[i].nodeName)!=-1)
							{
								parent.insertBefore(newel,element.childNodes[i]);
								cell.ELEMENT = newel;
								cell.className='columnchild_checked';
								checkbox.checked = true;
								return;
							}
					}
					// Did not find a tag which should be located after the newel, so we can appendChild
					parent.appendChild(newel);
				}

			}
			cell.ELEMENT = newel;
			cell.className='columnchild_checked';
			checkbox.checked = true;
		}else{
			cell.ELEMENT.parentNode.removeChild(cell.ELEMENT);
			cell.ELEMENT = null;
			cell.className='columnchild_unchecked';
			checkbox.checked = false;
		}
	}




	/*
 	* Appends to the end or inserts if index given
 	*/
	this.add = function(newel,index)
	{
		// add it to the dic
		var id=this.map.length;
		this.map[id]=newel;

		for (var child=0; child<newel.childNodes.length; child++){
			if (newel.childNodes[child].nodeType==1 && newel.childNodes[child].nodeName==this.COLUMN_CHILD_TYPE[0]){
				// add column child
				var col_child = new Array();
				for (var at=2; at<this.COLUMN_CHILD_TYPE.length; at++){
					col_child[col_child.length]=newel.childNodes[child].getAttribute(this.COLUMN_CHILD_TYPE[at][0]);
				}
				this.addColumnChild(col_child);
			}
		}

		if (index==-1){
			var nextRow = this.TABLE.tBodies[0].rows.length-1;
		}else{
			var nextRow = index;
		}
		// add the row
		var row = this.TABLE.tBodies[0].insertRow(nextRow);


		// cell 0 - insert
		var cell0 = row.insertCell(0);
		var raEl = this.document.createElement('img');
		raEl.setAttribute('src', 'images/insert.png');
		raEl.setAttribute('title', 'Insert');
		raEl.setAttribute('height', '25');
		raEl.setAttribute('width', '25');
		cell0.className='buttoncell';
		cell0.onclick = new Function("evt", this.MAP_TABLELIST_IDENTIFIER+"["+String(this.ID)+"]"+".insertNew(this.parentNode)");
		cell0.appendChild(raEl);

		// cell1 - see children
		var cell1 = row.insertCell(1);
		raEl = this.document.createElement('img');
		raEl.setAttribute('src', 'images/expand.png');
		raEl.setAttribute('title', 'See children');
		raEl.setAttribute('height', '20');
		raEl.setAttribute('width', '20');
		cell1.className='buttoncell';
		cell1.onclick = new Function("evt", this.MAP_TABLELIST_IDENTIFIER+"["+String(this.ID)+"].changeVisibility("+String(id)+")");
		cell1.appendChild(raEl);

		for (var i=2; i<this.DATA_TYPE.length; i++){
			var celli = row.insertCell(i);
			if (hasAttribute(newel,this.DATA_TYPE[i][0])){
				if (newel.getAttribute(this.DATA_TYPE[i][0])!=''){
					var txt = this.document.createTextNode(newel.getAttribute(this.DATA_TYPE[i][0]));
					celli.className='doubletable_item';
				}else{
					var txt = this.document.createTextNode('*TO FILL*');
					celli.className='doubletable_item';
				}
			}else{
				var txt = this.document.createTextNode('-');
				celli.className='empty';
			}

			celli.ondblclick = new Function("evt", "editCell(this,false)");
			celli.ELEMENT_TYPE = this.DATA_TYPE[i];
			celli.ELEMENT_ID = id;
			celli.MAP = this.map;
			celli.fixedClass = 'doubletable_item';
			celli.appendChild(txt);
		}

		// delete
		var cellX = row.insertCell(this.DATA_TYPE.length);
		var inEl = this.document.createElement('img');
		inEl.setAttribute('src', 'images/delete.png');
		inEl.setAttribute('title', 'Delete');
		inEl.setAttribute('height', '25');
		inEl.setAttribute('width', '25');
		cellX.onclick = new Function('evt',this.MAP_TABLELIST_IDENTIFIER+'['+String(this.ID)+']'+'.deleteElem('+String(id)+')');
		cellX.className='buttoncell';
		cellX.appendChild(inEl);

		var cell;
		var check;
		for (var i=0; i<this.COLUMN_ELEMENTS.length; i++){
			cell = row.insertCell(row.cells.length);
			cell.colSpan=this.COLUMN_CHILD_TYPE.length-1;
			cell.ATTRIBUTES = this.COLUMN_ELEMENTS[i];

			check = this.document.createElement('input');
			check.setAttribute('type','checkbox');
			cell.appendChild(check);
			check.onclick = new Function("evt", this.MAP_TABLELIST_IDENTIFIER+"["+String(this.ID)+"]"+".onclickColumnChildCell(this.parentNode)");

			if (newel.childNodes.length>0){
				var elem = null;
				var cleanUp = false;
				for (var child=0; child<newel.childNodes.length; child++){
					if (newel.childNodes[child].nodeType==1 && newel.childNodes[child].nodeName==this.COLUMN_CHILD_TYPE[0]){
						var isChecked = true;
						for (var at=2; at<this.COLUMN_CHILD_TYPE.length; at++){
							if (newel.childNodes[child].getAttribute(this.COLUMN_CHILD_TYPE[at][0])!=this.COLUMN_ELEMENTS[i][at-2]){
								isChecked=false;
								break;
							}
						}
						if (isChecked){
							if (cleanUp){
								newel.removeChild(newel.childNodes[child]);
							}else{
								cell.ELEMENT = newel.childNodes[child];
								cleanUp=true;
							}
						}
					}
				}
				if (cleanUp){
					check.checked = true;
					cell.className='columnchild_checked';
				}else{
					cell.className='columnchild_unchecked';
				}
			}else{
				cell.className='columnchild_unchecked';
			}
		}

		row.ELEMENT_ID=id;
		row.SUBLISTS=new Array();

		for (var i=0; i<this.CHILDREN_TYPES.length; i++){
			nextRow +=1;
			var childRow = this.TABLE.tBodies[0].insertRow(nextRow);

			cell = childRow.insertCell(0);
			cell.className='empty';
			txt = this.document.createTextNode('-');
			cell.appendChild(txt);
			childRow.appendChild(cell);


			cell = childRow.insertCell(1);
			cell.colSpan=this.DATA_TYPE.length-1;
			var tbl=new TableList(this.document,this.CHILDREN_TYPES[i],this,id,this.map,this.MAP_TABLELIST,this.MAP_TABLELIST_IDENTIFIER,this.xmlObj);
			tbl.initialize(cell);
			tbl.TABLE.className='subtable';
			row.SUBLISTS[row.SUBLISTS.length]=tbl;
			childRow.appendChild(cell);


			cell = childRow.insertCell(2);
			cell.className='empty';
			cell.colSpan=this.COLUMN_ELEMENTS.length*(this.COLUMN_CHILD_TYPE.length-1)+1;
			txt = this.document.createTextNode('-');
			cell.appendChild(txt);
			childRow.appendChild(cell);
		}

		// Hide children by default
		for (var l=0; l<this.CHILDREN_TYPES.length; l++){
			this.TABLE.tBodies[0].rows[row.sectionRowIndex+1+l].style.display="none";
		}
		this.updateSeeChildren(id,row);
		this.size++;
	}


	this.insertNew = function(row){
		// try to create the new element in the tree and check in cgi
		var newel=this.xmlObj.createElement(this.DATA_TYPE[0]);
		for (var i=2; i<this.DATA_TYPE.length; i++){
			if (this.DATA_TYPE[i][2].length>0){
				// default value
				newel.setAttribute(this.DATA_TYPE[i][0],this.DATA_TYPE[i][2][0]);
			}else if (this.DATA_TYPE[i][1]){
				// attribute is required, initialize it with a dummy string
				newel.setAttribute(this.DATA_TYPE[i][0],'*TO FILL*');
			}
		}

		nextNode=this.map[this.TABLE.tBodies[0].rows[row.sectionRowIndex].ELEMENT_ID]
		this.map[this.PARENT_ID].insertBefore(newel,nextNode);

		// add it to the table
		this.add(newel,row.sectionRowIndex);
	}


	this.addNew = function(){
		// try to create the new element in the tree and check in cgi
		var newel=this.xmlObj.createElement(this.DATA_TYPE[0]);
		for (var i=2; i<this.DATA_TYPE.length; i++){
			if (this.DATA_TYPE[i][2].length>0){
				// default value
				newel.setAttribute(this.DATA_TYPE[i][0],this.DATA_TYPE[i][2][0]);
			}else if (this.DATA_TYPE[i][1]){
				// attribute is required, initialize it with a dummy string
				newel.setAttribute(this.DATA_TYPE[i][0],'*TO FILL*');
			}
		}
		if (this.DATA_TYPE[1].length<=1){
			this.map[this.PARENT_ID].appendChild(newel);
		}else{
			var sequenceIndex = indexOf(this.DATA_TYPE[1],this.DATA_TYPE[0]);
			var nextTags=new Array();
			for (var i=sequenceIndex+1;i<this.DATA_TYPE[1].length; i++){
				nextTags[nextTags.length]=this.DATA_TYPE[1][i];
			}
			if (nextTags.length==0){
				// Last position in sequence : we can appendChild
				this.map[this.PARENT_ID].appendChild(newel);
			}else{
				// If we find a tag that is in nextTags, we insert before it, else we appendChild
				for (var i=0; i<this.map[this.PARENT_ID].childNodes.length; i++){
					if (	this.map[this.PARENT_ID].childNodes[i].nodeType==1 &&
						indexOf(nextTags,this.map[this.PARENT_ID].childNodes[i].nodeName)!=-1)
						{
							this.map[this.PARENT_ID].insertBefore(newel,this.map[this.PARENT_ID].childNodes[i]);
							this.add(newel,-1);
							return;
						}
				}
				// Did not find a tag which should be located after the newel, so we can appendChild
				this.map[this.PARENT_ID].appendChild(newel);
			}

		}

		// add it to the table
		this.add(newel,-1);
	}

	this.deleteElem = function(id)
	{
		if (!askDeleteConfirmation()) return;
		//try to remove the element from the tree and check in cgi
		this.map[id].parentNode.removeChild(this.map[id]);

		// remove it from the table, and remove the child rows
		for (var i=0; i<this.TABLE.tBodies[0].rows.length; i++) {
			if (this.TABLE.tBodies[0].rows[i].ELEMENT_ID && this.TABLE.tBodies[0].rows[i].ELEMENT_ID==id) {
				for (var k=0; k<this.CHILDREN_TYPES.length; k++){
					this.TABLE.tBodies[0].rows[i].parentNode.deleteRow(this.TABLE.tBodies[0].rows[i].sectionRowIndex+1);
				}
				this.TABLE.tBodies[0].rows[i].parentNode.deleteRow(this.TABLE.tBodies[0].rows[i].sectionRowIndex);
				break;
			}
		}
		this.size--;
	}

	this.updateSeeChildren = function(id,row){
		var r=null;
		if (row==null){
			for (var i=0; i<this.TABLE.tBodies[0].rows.length; i++) {
				if (this.TABLE.tBodies[0].rows[i].ELEMENT_ID && this.TABLE.tBodies[0].rows[i].ELEMENT_ID==id) {
					r=this.TABLE.tBodies[0].rows[i];
					break;
				}
			}
		}else{
			r=row;
		}
		var image=r.childNodes[1].childNodes[0];
		for (var i=0; i<r.SUBLISTS.length; i++){
			if (r.SUBLISTS[i].size>0){
				image.setAttribute('src','images/expand.png');
				image.setAttribute('title','See children');
				return true;
			}
		}
		image.setAttribute('src','images/expand_empty.png');
		image.setAttribute('title','Add children');
		return false;
	}


	this.changeVisibility = function(id){
		for (var i=0; i<this.TABLE.tBodies[0].rows.length; i++) {
			if (this.TABLE.tBodies[0].rows[i].ELEMENT_ID && this.TABLE.tBodies[0].rows[i].ELEMENT_ID==id) {
				if (this.TABLE.tBodies[0].rows[this.TABLE.tBodies[0].rows[i].sectionRowIndex+1].style.display=="none"){
					for (var l=0; l<this.CHILDREN_TYPES.length; l++){
						this.TABLE.tBodies[0].rows[this.TABLE.tBodies[0].rows[i].sectionRowIndex+1+l].style.display="";
					}
				}else{
					for (var l=0; l<this.CHILDREN_TYPES.length; l++){
						this.TABLE.tBodies[0].rows[this.TABLE.tBodies[0].rows[i].sectionRowIndex+1+l].style.display="none";
					}
				}
			}
		}
	}

}











// Remember the cell being edited
var ce=null;
// Lock if the cell content is already being edited
var lock=false;

function editCell(cell,isColumnChild){
	if (lock) return;
	ce=cell;

	if (cell.ELEMENT_TYPE[3].length>0){
		// Enumeration, so we display a selectbox
		var raEl = cell.ownerDocument.createElement('select');
		var tempOp=null;

		// if it is a column child, all attributes MUST be required
		if ((!cell.ELEMENT_TYPE[1]) && (!isColumnChild)){
			// attribute is optional
			tempOp=cell.ownerDocument.createElement('option');
			tempOp.text='';
			try{
  				raEl.add(tempOp,null);
				// standards compliant
			}catch(ex){
  				raEl.add(tempOp);
				// IE only
 			}
		}
		for (var op in cell.ELEMENT_TYPE[3]){
			tempOp=cell.ownerDocument.createElement('option');
			tempOp.text=cell.ELEMENT_TYPE[3][op];
			try{
  				raEl.add(tempOp,null);
				// standards compliant
			}catch(ex){
  				raEl.add(tempOp);
				// IE only
 			}
		}

		// initialize the selected index
		for (var op=0; op<raEl.length; op++){
			if (raEl[op].text==cell.childNodes[0].data){
				raEl.selectedIndex = String(op);
			}
		}

	}else{
		// Free text, so we display an input
		var raEl = cell.ownerDocument.createElement('input');
		raEl.setAttribute('type', 'text');
		if (cell.className!='empty'){
			raEl.value=cell.childNodes[0].data;
		}

	}

	if (!isColumnChild){
		raEl.onblur = new Function("evt", "inputCommit()");
	}else{
		raEl.onblur = new Function("evt", "inputColumnCommit()");
	}
	cell.removeChild(cell.childNodes[0]);
	cell.appendChild(raEl);
	raEl.focus();
	lock=true;
}



function inputCommit(){
	if (!lock || ce==null) return;

	if (ce.ELEMENT_TYPE[3].length>0){
		// Enumeration, we get the data from a selectbox
		var input_text=ce.childNodes[0].options[ce.childNodes[0].selectedIndex].text;
	}else{
		// Free text, we get the data from an input field
		var input_text=ce.childNodes[0].value;
	}

	if (input_text.replace(/\s+/g,"")!=""){
		var raEl= ce.ownerDocument.createTextNode(input_text);
		ce.className= ce.fixedClass;
		ce.MAP[ce.ELEMENT_ID].setAttribute(ce.ELEMENT_TYPE[0],input_text);
	}else{
		// SEND ERROR MESSAGE IF ATTRIBUTE IS REQUIRED
		if (ce.ELEMENT_TYPE[1]){
			alert('This attribute is required, you must specify something');
			ce.childNodes[0].focus();
			return;
		}
		var raEl= ce.ownerDocument.createTextNode('-');
		ce.className='empty';
		ce.MAP[ce.ELEMENT_ID].removeAttribute(ce.ELEMENT_TYPE[0]);
	}
	ce.removeChild(ce.childNodes[0]);
	ce.appendChild(raEl);
	if (ce.DIV_ID){
		// update slot link and title
		var link = document.getElementById('link_'+String(ce.DIV_ID));
		link.innerHTML = 'Slot '+ce.MAP[ce.ELEMENT_ID].getAttribute('name');
		var title = document.getElementById('slot_title_'+String(ce.DIV_ID));
		title.innerHTML = 'Slot '+ce.MAP[ce.ELEMENT_ID].getAttribute('name');
	}
	lock=false;
	ce=null;
}



function inputColumnCommit(){
	if (!lock || ce==null) return;
	var input_text;

	if (ce.ELEMENT_TYPE[3].length>0){
		// Enumeration, we get the data from a selectbox
		input_text=ce.childNodes[0].options[ce.childNodes[0].selectedIndex].text;
	}else{
		// Free text, we get the data from an input field
		input_text=ce.childNodes[0].value;
	}

	if (input_text.replace(/\s+/g,"")==""){
		// SEND ERROR MESSAGE SINCE ATTRIBUTE MUST BE SPECIFIED (CHILD COLUMN ATTRIBUTES ARE REQUIRED)
		alert('This attribute is required, you must specify something');
		ce.childNodes[0].focus();
		return;
	}

	var attributes = ce.LIST.COLUMN_ELEMENTS[ce.LIST.getColumnChild(ce.ATTRIBUTES)];
	var oldText = attributes[ce.AT_INDEX];

	// update the array of column elements in the table
	attributes[ce.AT_INDEX]=input_text;

	// check that the dependence does not already exist
	for (var i=0;i<ce.LIST.COLUMN_ELEMENTS.length; i++){
		if (i!=ce.LIST.getColumnChild(ce.ATTRIBUTES)){
			var childAlreadyExists = true;
			for (var j=0; j<attributes.length; j++){
				if (ce.LIST.COLUMN_ELEMENTS[i][j]!=attributes[j]){
					childAlreadyExists=false;
					break;
				}
			}
			if (childAlreadyExists){
				alert('This dependence already exists');
				attributes[ce.AT_INDEX] = oldText;
				ce.childNodes[0].focus();
				return;
			}
		}
	}


	// edit elements in the tree
	var tbody = ce.TBODY;

	for (var row=0; row<tbody.rows.length ; row++){
		if (tbody.rows[row].ELEMENT_ID){
			// if the element has the column child we have to edit it

			var element = ce.MAP[tbody.rows[row].ELEMENT_ID];
			for (var child=0; child<element.childNodes.length; child++){
				if (element.childNodes[child].nodeType==1 && element.childNodes[child].nodeName==ce.WHOLE_TYPE[0]){
					var toEdit = true;
					for (var at=0; at<ce.WHOLE_TYPE.length-2; at++){
						if (element.childNodes[child].getAttribute(ce.WHOLE_TYPE[at+2][0])!=attributes[at]){
							toEdit=false;
							break;
						}
					}

					if (toEdit){
						element.childNodes[child].setAttribute(ce.ELEMENT_TYPE[0],input_text)
					}
				}
			}
		}
	}

	var raEl= ce.ownerDocument.createTextNode(input_text);
	ce.removeChild(ce.childNodes[0]);
	ce.appendChild(raEl);
	lock=false;
	ce=null;
}















// TYPE AND SEQUENCE DEFINITIONS

var SLOT_SEQUENCE = ['paths','cmtprojectpath','platforms','waitfor','cmtextratags','lblogin','runbefore','runafter','days','projects','envvars']
var SLOT_PROJECT_SEQUENCE = ['dependence','change']
var CONFIGURATION_SEQUENCE= ['general','slot']
var IGNORE_SEQUENCE= ['error','warning']
var MAILTO_SEQUENCE= ['mail','repository']


/*

A type is kinf of a copy of what is in the schema file (configuration.xsd).
The syntax is as follows :

TYPE =
[
	name_of_tag,
	sequence_in_parent,
	[name_of_attribute,is_required,[default_value],[enumerated_values]],
	... other attributes ...
]

The sequence_in_parent should contain at least the name of the tag, it is used to respect the order of appearance of the children in the parent tag if a sequence is defined

After the name of the tag and the sequence, we define one after the other the attributes of the type. An attribute is an array containing the name of the attribute, if it is required, its default value (if there's no default value then use []), and its enumerated values if this attribute has an enumeration (if there's no enumerated value then use [])

*/


var PARAMETER_TYPE=
[
	'parameter',
	['parameter'],
	['name',true,[],[]],
	['value',true,[],[]]
]

var ERROR_TYPE=
[
	'error',
	IGNORE_SEQUENCE,
	['type',false,[],[]],
	['value',true,[],[]]
]

var WARNING_TYPE=
[
	'warning',
	IGNORE_SEQUENCE,
	['type',false,[],[]],
	['value',true,[],[]]
]

var MAILTO_PROJECT_TYPE=
[
	'project',
	['project'],
	['name',true,[],[]]
]

var MAIL_TYPE=
[
	'mail',
	MAILTO_SEQUENCE,
	['address',true,[],[]],
	['edit',false,['false'],['false','true']],
	['parent',false,['false'],['false','true']]
]

var REPOSITORY_TYPE=
[
	'repository',
	MAILTO_SEQUENCE,
	['type',true,[],[]],
	['path',true,[],[]]
]

var BUILDERS_PROJECT_TYPE=
[
	'project',
	['project'],
	['name',true,[],[]],
	['path',true,[],[]]
]

var SLOT_TYPE=
[
	'slot',
	CONFIGURATION_SEQUENCE,
	['name',true,[],[]],
	['description',false,[],[]],
	['mails',false,[],['true','false']],
	['hidden',false,[],['true','false']],
	['computedependencies',false,[],['true','false']],
	['renice',false,[],[]],
	['disabled',false,[],['true','false']],
	['overwriteprojectlistbyxml',false,[],[]]
]

var SLOT_PROJECT_TYPE=
[
	'project',
	['project'],
	['name',true,[],[]],
	['tag',true,[],[]],
	['headofeverything',false,[],['true','false']],
	['docs',false,[],['true','false']],
	['disabled',false,[],['true','false']]
]

// ALL THE ATTRIBUTES OF A DEPENDENCE MUST BE REQUIRED FOR THE TABLE OF PROJECTS TO WORK PROPERLY
var DEPENDENCE_TYPE=
[
	'dependence',
	SLOT_PROJECT_SEQUENCE,
	['project',true,[],[]],
	['tag',true,[],[]]
]

var CHANGE_TYPE=
[
	'change',
	SLOT_PROJECT_SEQUENCE,
	['package',true,[],[]],
	['value',true,[],[]]
]

var ADDON_TYPE=
[
	'addon',
	SLOT_PROJECT_SEQUENCE,
	['package',true,[],[]],
	['value',true,[],[]]
]

var CMTEXTRATAGS_TYPE=
[
	'cmtextratags',
	SLOT_SEQUENCE,
	['value',true,[],[]]
]

var PLATFORM_TYPE=
[
	'platform',
	['platform'],
	['name',true,[],[]]
]

var PATH_TYPE=
[
	'path',
	['path'],
	['name',false,[],[]],
	['value',true,[],[]]
]

var CMPROJECTPATH_TYPE=
[
	'path',
	['path'],
	['value',true,[],[]]
]

var DAYS_TYPE=
[
	'days',
	SLOT_SEQUENCE,
	['mon',true,['true'],['true','false']],
	['tue',true,['true'],['true','false']],
	['wed',true,['true'],['true','false']],
	['thu',true,['true'],['true','false']],
	['fri',true,['true'],['true','false']],
	['sat',true,['true'],['true','false']],
	['sun',true,['true'],['true','false']]
]

var RUNAFTER_TYPE=
[
	'runafter',
	SLOT_SEQUENCE,
	['linux',false,[],[]],
	['win',false,[],[]],
	['mac',false,[],[]]
]

var RUNBEFORE_TYPE=
[
	'runbefore',
	SLOT_SEQUENCE,
	['linux',false,[],[]],
	['win',false,[],[]],
	['mac',false,[],[]]
]

var LBLOGIN_TYPE=
[
	'lblogin',
	SLOT_SEQUENCE,
	['linux',false,[],[]],
	['win',false,[],[]],
	['mac',false,[],[]]
]

var WAITFOR_TYPE=
[
	'waitfor',
	SLOT_SEQUENCE,
	['flag',true,[],[]]
]










function initialize(xmlObj,map){
	for (var i=0; i<xmlObj.childNodes.length; i++){
		if (xmlObj.childNodes[i].nodeType==1){
			map[map.length]=xmlObj.childNodes[i];
			switch(xmlObj.childNodes[i].nodeName){
				case 'slot':
					initializeSlot(map,map.length-1);
					break;
				case 'general':
					initializeGeneral(map,map.length-1);
					break;
				default:
					initialize(xmlObj.childNodes[i],map);
					break;
			}
		}
	}
}


var toggleIDs=0;
function nextDivID(){
	toggleIDs++;
	return toggleIDs;
}





function initializeGeneral(map,index){
	var general = map[index];

	// DIV FOR TITLE, CONTENT, ...

	var mainDiv = document.createElement('div');
	var id = nextDivID();
	mainDiv.id = 'general_'+String(id);

	var toggle = document.createElement('a');
	toggle.id = String(id)+'_toggle';
	toggle.innerHTML = '[&#171;]';
	toggle.className='toggle';
	toggle.setAttribute('href','#general_'+String(id));
	toggle.onclick = new Function('evt','return toggleMe('+String(id)+')');

	var title = document.createElement('h2');

	var text = document.createElement('font');
	text.className='slot_title';
	text.innerHTML = 'General';


	var top = document.createElement('a');
	top.setAttribute('href','#');
	top.innerHTML = '[ Top ]<hr/>';
	top.className='link_to_top';

	var div = document.createElement('div');
	div.id = String(id);

	title.appendChild(toggle);
	title.appendChild(text);
	title.appendChild(top);
	mainDiv.appendChild(title);
	mainDiv.appendChild(div);
	document.getElementById('general_div').appendChild(mainDiv);


	// SLOT LIST ELEMENT IN THE HEADER

	var link = document.createElement('a');
	link.id = 'link_'+String(id);
	link.setAttribute('href','#general_'+String(id));
	link.className='my_link';
	link.innerHTML = 'General';
	var list_elem = document.createElement('li');
	list_elem.id = 'link_container_'+String(id);
	list_elem.appendChild(link);
	document.getElementById('header').appendChild(list_elem);


	// DIVS FOR EACH GENERAL SECTION

	// PARAMETERS
	text = document.createElement('font');
	text.className='slot_section';
	text.innerHTML = 'Parameters';
	var maindiv_parameters = document.createElement('div');
	var div_parameters = document.createElement('div');
	var toggle_parameters = document.createElement('a');
	maindiv_parameters.className='slot_content';
	maindiv_parameters.appendChild(toggle_parameters);
	maindiv_parameters.appendChild(text);
	maindiv_parameters.appendChild(div_parameters);
	div.appendChild(maindiv_parameters);

	// ERRORS
	text = document.createElement('font');
	text.className='slot_section';
	text.innerHTML = 'Errors';
	var maindiv_errors = document.createElement('div');
	var div_errors = document.createElement('div');
	var toggle_errors = document.createElement('a');
	maindiv_errors.className='slot_content';
	maindiv_errors.appendChild(toggle_errors);
	maindiv_errors.appendChild(text);
	maindiv_errors.appendChild(div_errors);
	div.appendChild(maindiv_errors);

	// WARNINGS
	text = document.createElement('font');
	text.className='slot_section';
	text.innerHTML = 'Warnings';
	var maindiv_warnings = document.createElement('div');
	var div_warnings = document.createElement('div');
	var toggle_warnings = document.createElement('a');
	maindiv_warnings.className='slot_content';
	maindiv_warnings.appendChild(toggle_warnings);
	maindiv_warnings.appendChild(text);
	maindiv_warnings.appendChild(div_warnings);
	div.appendChild(maindiv_warnings);


	// MAILTO
	text = document.createElement('font');
	text.className='slot_section';
	text.innerHTML = 'Mailto';
	var maindiv_mailto = document.createElement('div');
	var div_mailto = document.createElement('div');
	var toggle_mailto = document.createElement('a');
	maindiv_mailto.className='slot_content';
	maindiv_mailto.appendChild(toggle_mailto);
	maindiv_mailto.appendChild(text);
	maindiv_mailto.appendChild(div_mailto);
	div.appendChild(maindiv_mailto);



	for (var i=0; i< general.childNodes.length; i++){
		if (general.childNodes[i].nodeType==1){
			map[map.length]=general.childNodes[i];
			switch(general.childNodes[i].nodeName){
				case 'parameters':
					var list = new TableList(document,PARAMETER_TYPE,null,map.length-1,map_ID,map_TABLELIST,'map_TABLELIST',xmlObj);
					toggle_parameters.innerHTML = '[&#187;]';
					div_parameters.style.display="none";
					id = nextDivID();
					toggle_parameters.id = String(id)+'_toggle';
					maindiv_parameters.id = 'general_content_'+String(id)
					toggle_parameters.setAttribute('href','#general_content_'+String(id));
					toggle_parameters.onclick = new Function('evt','return toggleMe('+String(id)+')');
					div_parameters.id = String(id);
					list.initialize(div_parameters);
					break;
				case 'ignore':
					var ignore_id = map.length-1;

					var list = new TableList(document,ERROR_TYPE,null,ignore_id,map_ID,map_TABLELIST,'map_TABLELIST',xmlObj);
					toggle_errors.innerHTML = '[&#187;]';
					div_errors.style.display="none";
					id = nextDivID();
					toggle_errors.id = String(id)+'_toggle';
					maindiv_errors.id = 'general_content_'+String(id)
					toggle_errors.setAttribute('href','#general_content_'+String(id));
					toggle_errors.onclick = new Function('evt','return toggleMe('+String(id)+')');
					div_errors.id = String(id);
					list.initialize(div_errors);

					list = new TableList(document,WARNING_TYPE,null,ignore_id,map_ID,map_TABLELIST,'map_TABLELIST',xmlObj);
					toggle_warnings.innerHTML = '[&#187;]';
					div_warnings.style.display="none";
					id = nextDivID();
					toggle_warnings.id = String(id)+'_toggle';
					maindiv_warnings.id = 'general_content_'+String(id)
					toggle_warnings.setAttribute('href','#general_content_'+String(id));
					toggle_warnings.onclick = new Function('evt','return toggleMe('+String(id)+')');
					div_warnings.id = String(id);
					list.initialize(div_warnings);

					break;
				case 'mailto':
					var list = new DoubleTableList(document,MAILTO_PROJECT_TYPE,[MAIL_TYPE,REPOSITORY_TYPE],map.length-1,map_ID,
																map_TABLELIST,'map_TABLELIST',xmlObj);
					toggle_mailto.innerHTML = '[&#187;]';
					div_mailto.style.display="none";
					id = nextDivID();
					toggle_mailto.id = String(id)+'_toggle';
					maindiv_mailto.id = 'general_content_'+String(id)
					toggle_mailto.setAttribute('href','#general_content_'+String(id));
					toggle_mailto.onclick = new Function('evt','return toggleMe('+String(id)+')');
					div_mailto.id = String(id);
					list.initialize(div_mailto);
					break;
				case 'builders':
					// BUILDERS
					text = document.createElement('font');
					text.className='slot_section';
					text.innerHTML = 'Builders';
					var maindiv_builders = document.createElement('div');
					var div_builders = document.createElement('div');
					var toggle_builders = document.createElement('a');
					maindiv_builders.className='slot_content';
					maindiv_builders.appendChild(toggle_builders);
					maindiv_builders.appendChild(text);
					maindiv_builders.appendChild(div_builders);
					div.appendChild(maindiv_builders);

					var list = new TableList(document,BUILDERS_PROJECT_TYPE,null,map.length-1,map_ID,map_TABLELIST,'map_TABLELIST',xmlObj);
					toggle_builders.innerHTML = '[&#187;]';
					div_builders.style.display="none";
					id = nextDivID();
					toggle_builders.id = String(id)+'_toggle';
					maindiv_builders.id = 'general_content_'+String(id)
					toggle_builders.setAttribute('href','#general_content_'+String(id));
					toggle_builders.onclick = new Function('evt','return toggleMe('+String(id)+')');
					div_builders.id = String(id);
					list.initialize(div_builders);
					break;
			}
		}
	}
}









function SlotInfo(doc,data_type,div_id,element_id,map)
{
	this.document=doc; // remember the document's name to know where we can find the table
	this.TABLE=null; // table in the document
	this.DATA_TYPE=data_type; // which type of data will be present in the list
	this.ELEMENT_ID=element_id; // the slot xml element
	this.DIV_ID=div_id; // the id of div and link to dynamically change the title and the item in the header
	this.map=map;


	/*
	 * Creates the THead and TBody and fills the default rows : header
	 */
	this.initialize = function(container){
		var slot = this.map[this.ELEMENT_ID];

		this.TABLE = this.document.createElement("table");
		container.appendChild(this.TABLE);

		this.TABLE.createTHead();
		var tblBody = this.document.createElement("tbody");
		this.TABLE.appendChild(tblBody);

		// Add title - 1st row
		var title = this.TABLE.tHead.insertRow(0);
		var cell = title.insertCell(0);
		cell.colSpan=2;
		cell.className='subtitle';


		var txt = this.document.createTextNode(this.DATA_TYPE[0].toUpperCase());
		cell.appendChild(txt);
		title.appendChild(cell);

		var rowi;
		for (var i=2; i<this.DATA_TYPE.length; i++){
			rowi = this.TABLE.tBodies[0].insertRow(this.TABLE.tBodies[0].rows.length);

			// cell 0 : attribute name
			cell = rowi.insertCell(0);
			cell.className='attribute';
			txt = this.document.createTextNode(this.DATA_TYPE[i][0]);
			cell.appendChild(txt);
			rowi.appendChild(cell);


			// cell 1 : attribute value
			cell = rowi.insertCell(1);
			if (hasAttribute(slot,this.DATA_TYPE[i][0])){
				if (slot.getAttribute(this.DATA_TYPE[i][0])!=''){
					var txt = this.document.createTextNode(slot.getAttribute(this.DATA_TYPE[i][0]));
					cell.className='value';
				}else{
					var txt = this.document.createTextNode('*TO FILL*');
					cell.className='value';
				}
			}else{
				txt = this.document.createTextNode('-');
				cell.className='empty';
			}
			cell.ondblclick = new Function("evt", "editCell(this,false)");
			cell.ELEMENT_TYPE = this.DATA_TYPE[i];
			cell.ELEMENT_ID = this.ELEMENT_ID;
			cell.MAP = this.map;
			if(this.DATA_TYPE[i][0]=='name'){
				// to be able to synchronize titles and links when we change the name
				cell.DIV_ID = this.DIV_ID;
			}
			cell.fixedClass = 'value';
			cell.appendChild(txt);

		}
	}
}




function deleteSlot(div_id,element_id,map){
	if (!askDeleteConfirmation()) return;
	var div = document.getElementById('slot_'+String(div_id));
	var link_container = document.getElementById('link_container_'+String(div_id));
	var element = map[element_id];
	element.parentNode.removeChild(element);
	div.parentNode.removeChild(div);
	link_container.parentNode.removeChild(link_container);
}




function addSlot(map,xmlObj,slot_name,slot_path,slot_cmtpath,slot_platform,slot_days){
	var slot=xmlObj.createElement('slot');
	slot.setAttribute('name',slot_name);

	// at least one path
	var paths=xmlObj.createElement('paths');
	var path=xmlObj.createElement('path');
	path.setAttribute('value',slot_path);
	paths.appendChild(path);

	// at least one cmtpath
	var cmtpaths=xmlObj.createElement('cmtprojectpath');
	var cmtpath=xmlObj.createElement('path');
	cmtpath.setAttribute('value',slot_cmtpath);
	cmtpaths.appendChild(cmtpath);

	// at least one platform
	var platforms=xmlObj.createElement('platforms');
	var platform=xmlObj.createElement('platform');
	platform.setAttribute('name',slot_platform);
	platforms.appendChild(platform);

	// days
	var days=xmlObj.createElement('days');
	for (var i=0; i<slot_days.length; i++){
		days.setAttribute(slot_days[i][0],slot_days[i][1]);
	}

	// projects
	var projects=xmlObj.createElement('projects');

	slot.appendChild(paths);
	slot.appendChild(cmtpaths);
	slot.appendChild(platforms);
	slot.appendChild(days);
	slot.appendChild(projects);


	// add to the tree and initialize tables
	map[map.length]=slot;
	map[0].appendChild(slot);
	initializeSlot(map,map.length-1);
}







function initializeSlot(map,index){
	var mother_slot = map[index];


	// SLOT DIVS FOR TITLE, CONTENT, ...

	var mainDiv = document.createElement('div');
	var id = nextDivID();
	mainDiv.id = 'slot_'+String(id);

	var toggle = document.createElement('a');
	toggle.id = String(id)+'_toggle';
	toggle.innerHTML = '[&#171;]';
	toggle.className='toggle';
	toggle.setAttribute('href','#slot_'+String(id));
	toggle.onclick = new Function('evt','return toggleMe('+String(id)+')');

	var title = document.createElement('h2');

	var text = document.createElement('font');
	text.className='slot_title';
	text.id = 'slot_title_'+String(id);
	text.innerHTML = 'Slot '+mother_slot.getAttribute('name');

	var del = document.createElement('img');
	del.setAttribute('src', 'images/delete.png');
	del.setAttribute('title', 'Delete');
	del.setAttribute('height', '20');
	del.setAttribute('width', '20');
	del.className='delete_slot';
	del.onclick = new Function('evt','deleteSlot('+String(id)+','+String(index)+',map_ID)');


	var top = document.createElement('a');
	top.setAttribute('href','#');
	top.innerHTML = '[ Top ]<hr/>';
	top.className='link_to_top';

	var div = document.createElement('div');
	div.id = String(id);

	title.appendChild(toggle);
	title.appendChild(text);
	title.appendChild(del);
	title.appendChild(top);
	mainDiv.appendChild(title);
	mainDiv.appendChild(div);
	document.getElementById('slots_div').appendChild(mainDiv);


	// SLOT LIST ELEMENT IN THE HEADER

	var link = document.createElement('a');
	link.id = 'link_'+String(id);
	link.setAttribute('href','#slot_'+String(id));
	link.className='my_link';
	link.innerHTML = 'Slot '+mother_slot.getAttribute('name');
	var list_elem = document.createElement('li');
	list_elem.id = 'link_container_'+String(id);
	list_elem.appendChild(link);
	document.getElementById('header').appendChild(list_elem);


	// DIVS FOR EACH SLOT SECTION

	var div_slotinfo = document.createElement('div');
	div_slotinfo.className='slot_content';
	var ls_slotinfo = new SlotInfo(document,SLOT_TYPE,id,index,map);
	ls_slotinfo.initialize(div_slotinfo);
	div.appendChild(div_slotinfo);


	text = document.createElement('font');
	text.className='slot_section';
	text.innerHTML = 'Paths';
	var maindiv_paths = document.createElement('div');
	var div_paths = document.createElement('div');
	var toggle_paths = document.createElement('a');
	maindiv_paths.className='slot_content';
	maindiv_paths.appendChild(toggle_paths);
	maindiv_paths.appendChild(text);
	maindiv_paths.appendChild(div_paths);
	div.appendChild(maindiv_paths);

	text = document.createElement('font');
	text.className='slot_section';
	text.innerHTML = 'Cmtprojectpaths';
	var maindiv_cmtprojectpaths = document.createElement('div');
	var div_cmtprojectpaths = document.createElement('div');
	var toggle_cmtprojectpaths = document.createElement('a');
	maindiv_cmtprojectpaths.className='slot_content';
	maindiv_cmtprojectpaths.appendChild(toggle_cmtprojectpaths);
	maindiv_cmtprojectpaths.appendChild(text);
	maindiv_cmtprojectpaths.appendChild(div_cmtprojectpaths);
	div.appendChild(maindiv_cmtprojectpaths);

	text = document.createElement('font');
	text.className='slot_section';
	text.innerHTML = 'Platforms';
	var maindiv_platforms = document.createElement('div');
	var div_platforms = document.createElement('div');
	var toggle_platforms = document.createElement('a');
	maindiv_platforms.className='slot_content';
	maindiv_platforms.appendChild(toggle_platforms);
	maindiv_platforms.appendChild(text);
	maindiv_platforms.appendChild(div_platforms);
	div.appendChild(maindiv_platforms);

	text = document.createElement('font');
	text.className='slot_section';
	text.innerHTML = 'Waitfor';
	var maindiv_waitfor = document.createElement('div');
	var div_waitfor = document.createElement('div');
	var toggle_waitfor = document.createElement('a');
	maindiv_waitfor.className='slot_content';
	maindiv_waitfor.appendChild(toggle_waitfor);
	maindiv_waitfor.appendChild(text);
	maindiv_waitfor.appendChild(div_waitfor);
	div.appendChild(maindiv_waitfor);

	var ls_waitfor = new TableList(document,WAITFOR_TYPE,null,index,map_ID,map_TABLELIST,'map_TABLELIST',xmlObj);
	toggle_waitfor.innerHTML = '[&#187;]';
	div_waitfor.style.display="none";
	id = nextDivID();
	toggle_waitfor.id = String(id)+'_toggle';
	maindiv_waitfor.id = 'slot_content_'+String(id)
	toggle_waitfor.setAttribute('href','#slot_content_'+String(id));
	toggle_waitfor.onclick = new Function('evt','return toggleMe('+String(id)+')');
	div_waitfor.id = String(id);
	ls_waitfor.initialize(div_waitfor);

	text = document.createElement('font');
	text.className='slot_section';
	text.innerHTML = 'Cmtextratags';
	var maindiv_cmtextra = document.createElement('div');
	var div_cmtextra = document.createElement('div');
	var toggle_cmtextra = document.createElement('a');
	maindiv_cmtextra.className='slot_content';
	maindiv_cmtextra.appendChild(toggle_cmtextra);
	maindiv_cmtextra.appendChild(text);
	maindiv_cmtextra.appendChild(div_cmtextra);
	div.appendChild(maindiv_cmtextra);

	var ls_cmtextra = new TableList(document,CMTEXTRATAGS_TYPE,null,index,map_ID,map_TABLELIST,'map_TABLELIST',xmlObj);
	toggle_cmtextra.innerHTML = '[&#187;]';
	div_cmtextra.style.display="none";
	id = nextDivID();
	toggle_cmtextra.id = String(id)+'_toggle';
	maindiv_cmtextra.id = 'slot_content_'+String(id)
	toggle_cmtextra.setAttribute('href','#slot_content_'+String(id));
	toggle_cmtextra.onclick = new Function('evt','return toggleMe('+String(id)+')');
	div_cmtextra.id = String(id);
	ls_cmtextra.initialize(div_cmtextra);

	text = document.createElement('font');
	text.className='slot_section';
	text.innerHTML = 'Lblogin';
	var maindiv_lblogin = document.createElement('div');
	var div_lblogin = document.createElement('div');
	var toggle_lblogin = document.createElement('a');
	maindiv_lblogin.className='slot_content';
	maindiv_lblogin.appendChild(toggle_lblogin);
	maindiv_lblogin.appendChild(text);
	maindiv_lblogin.appendChild(div_lblogin);
	div.appendChild(maindiv_lblogin);

	var ls_lblogin = new TableList(document,LBLOGIN_TYPE,null,index,map_ID,map_TABLELIST,'map_TABLELIST',xmlObj);
	toggle_lblogin.innerHTML = '[&#187;]';
	div_lblogin.style.display="none";
	id = nextDivID();
	toggle_lblogin.id = String(id)+'_toggle';
	maindiv_lblogin.id = 'slot_content_'+String(id)
	toggle_lblogin.setAttribute('href','#slot_content_'+String(id));
	toggle_lblogin.onclick = new Function('evt','return toggleMe('+String(id)+')');
	div_lblogin.id = String(id);
	ls_lblogin.initialize(div_lblogin);

	text = document.createElement('font');
	text.className='slot_section';
	text.innerHTML = 'Runbefore';
	var maindiv_runbefore = document.createElement('div');
	var div_runbefore = document.createElement('div');
	var toggle_runbefore = document.createElement('a');
	maindiv_runbefore.className='slot_content';
	maindiv_runbefore.appendChild(toggle_runbefore);
	maindiv_runbefore.appendChild(text);
	maindiv_runbefore.appendChild(div_runbefore);
	div.appendChild(maindiv_runbefore);

	var ls_runbefore = new TableList(document,RUNBEFORE_TYPE,null,index,map_ID,map_TABLELIST,'map_TABLELIST',xmlObj);
	toggle_runbefore.innerHTML = '[&#187;]';
	div_runbefore.style.display="none";
	id = nextDivID();
	toggle_runbefore.id = String(id)+'_toggle';
	maindiv_runbefore.id = 'slot_content_'+String(id)
	toggle_runbefore.setAttribute('href','#slot_content_'+String(id));
	toggle_runbefore.onclick = new Function('evt','return toggleMe('+String(id)+')');
	div_runbefore.id = String(id);
	ls_runbefore.initialize(div_runbefore);

	text = document.createElement('font');
	text.className='slot_section';
	text.innerHTML = 'Runafter';
	var maindiv_runafter = document.createElement('div');
	var div_runafter = document.createElement('div');
	var toggle_runafter = document.createElement('a');
	maindiv_runafter.className='slot_content';
	maindiv_runafter.appendChild(toggle_runafter);
	maindiv_runafter.appendChild(text);
	maindiv_runafter.appendChild(div_runafter);
	div.appendChild(maindiv_runafter);

	var ls_runafter = new TableList(document,RUNAFTER_TYPE,null,index,map_ID,map_TABLELIST,'map_TABLELIST',xmlObj);
	toggle_runafter.innerHTML = '[&#187;]';
	div_runafter.style.display="none";
	id = nextDivID();
	toggle_runafter.id = String(id)+'_toggle';
	maindiv_runafter.id = 'slot_content_'+String(id)
	toggle_runafter.setAttribute('href','#slot_content_'+String(id));
	toggle_runafter.onclick = new Function('evt','return toggleMe('+String(id)+')');
	div_runafter.id = String(id);
	ls_runafter.initialize(div_runafter);

	text = document.createElement('font');
	text.className='slot_section';
	text.innerHTML = 'Days';
	var maindiv_days = document.createElement('div');
	var div_days = document.createElement('div');
	var toggle_days = document.createElement('a');
	maindiv_days.className='slot_content';
	maindiv_days.appendChild(toggle_days);
	maindiv_days.appendChild(text);
	maindiv_days.appendChild(div_days);
	div.appendChild(maindiv_days);

	var ls_days = new SimpleTableList(document,DAYS_TYPE,index,map_ID);
	toggle_days.innerHTML = '[&#187;]';
	div_days.style.display="none";
	id = nextDivID();
	toggle_days.id = String(id)+'_toggle';
	maindiv_days.id = 'slot_content_'+String(id)
	toggle_days.setAttribute('href','#slot_content_'+String(id));
	toggle_days.onclick = new Function('evt','return toggleMe('+String(id)+')');
	div_days.id = String(id);
	ls_days.initialize(div_days);


	text = document.createElement('font');
	text.className='slot_section';
	text.innerHTML = 'Projects';
	var maindiv_projects = document.createElement('div');
	var div_projects = document.createElement('div');
	var toggle_projects = document.createElement('a');
	maindiv_projects.className='slot_content';
	maindiv_projects.appendChild(toggle_projects);
	maindiv_projects.appendChild(text);
	maindiv_projects.appendChild(div_projects);
	div.appendChild(maindiv_projects);

	text = document.createElement('font');
	text.className='slot_section';
	text.innerHTML = 'EnvironmentVariables';
	var maindiv_envvars = document.createElement('div');
	var div_envvars = document.createElement('div');
	var toggle_envvars = document.createElement('a');
	maindiv_projects.className='slot_content';
	maindiv_projects.appendChild(toggle_envvars);
	maindiv_projects.appendChild(text);
	maindiv_projects.appendChild(div_envvars);
	div.appendChild(maindiv_envvars);



	for (var i=0; i< mother_slot.childNodes.length; i++){
		if (mother_slot.childNodes[i].nodeType==1){
			map[map.length]=mother_slot.childNodes[i];
			switch(mother_slot.childNodes[i].nodeName){
				case 'paths':
					var list = new TableList(document,PATH_TYPE,null,map.length-1,map_ID,map_TABLELIST,'map_TABLELIST',xmlObj);
					toggle_paths.innerHTML = '[&#187;]';
					div_paths.style.display="none";
					id = nextDivID();
					toggle_paths.id = String(id)+'_toggle';
					maindiv_paths.id = 'slot_content_'+String(id)
					toggle_paths.setAttribute('href','#slot_content_'+String(id));
					toggle_paths.onclick = new Function('evt','return toggleMe('+String(id)+')');
					div_paths.id = String(id);
					list.initialize(div_paths);
					break;
				case 'cmtprojectpath':
					var list = new TableList(document,CMPROJECTPATH_TYPE,null,map.length-1,map_ID,map_TABLELIST,'map_TABLELIST',xmlObj);
					toggle_cmtprojectpaths.innerHTML = '[&#187;]';
					div_cmtprojectpaths.style.display="none";
					id = nextDivID();
					toggle_cmtprojectpaths.id = String(id)+'_toggle';
					maindiv_cmtprojectpaths.id = 'slot_content_'+String(id)
					toggle_cmtprojectpaths.setAttribute('href','#slot_content_'+String(id));
					toggle_cmtprojectpaths.onclick = new Function('evt','return toggleMe('+String(id)+')');
					div_cmtprojectpaths.id = String(id);
					list.initialize(div_cmtprojectpaths);
					break;
				case 'platforms':
					var list = new TableList(document,PLATFORM_TYPE,null,map.length-1,map_ID,map_TABLELIST,'map_TABLELIST',xmlObj);
					toggle_platforms.innerHTML = '[&#187;]';
					div_platforms.style.display="none";
					id = nextDivID();
					toggle_platforms.id = String(id)+'_toggle';
					maindiv_platforms.id = 'slot_content_'+String(id)
					toggle_platforms.setAttribute('href','#slot_content_'+String(id));
					toggle_platforms.onclick = new Function('evt','return toggleMe('+String(id)+')');
					div_platforms.id = String(id);
					list.initialize(div_platforms);
					break;
				case 'projects':
				        var list = new ProjectTableList(document,SLOT_PROJECT_TYPE,[CHANGE_TYPE, ADDON_TYPE],DEPENDENCE_TYPE,map.length-1,map_ID,
																map_TABLELIST,'map_TABLELIST',xmlObj);
					toggle_projects.innerHTML = '[&#171;]';
					id = nextDivID();
					toggle_projects.id = String(id)+'_toggle';
					maindiv_projects.id = 'slot_content_'+String(id)
					toggle_projects.setAttribute('href','#slot_content_'+String(id));
					toggle_projects.onclick = new Function('evt','return toggleMe('+String(id)+')');
					div_projects.id = String(id);
					list.initialize(div_projects);
					break;
			}
		}
	}
}



function toggleMe(a){
	var e=document.getElementById(a);
	var t=document.getElementById(a+"_toggle")
	if(!e)return true;
		if(e.style.display=="none"){
			e.style.display=""
			t.innerHTML = '[&#171;]'
		}else{
			e.style.display='none'
			t.innerHTML = '[&#187;]'
		}
	return false;
}
